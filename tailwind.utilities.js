const tech = require('./site/globals/tech.json')

let output = {}

const imagePath = '../images/logos/'
const prefix = 'tech-'

for (const [key, value] of Object.entries(tech)) {
  let className = `.${prefix}${key}`

  output[className] = {
    background: `url("${imagePath}${key}.${value.ext}")`,
    width: `${tech[key].width}`
  }
  if (value.backgroundSize)
    output[className].backgroundSize = tech[key].backgroundSize
  if (value.dark)
    output[`.dark ${className}`] = {
      background: `url("${imagePath}${key}dark.${value.ext}")`
    }
}

module.exports = output
