require('dotenv').config()

const htmlmin = require('html-minifier')

const lazyImagesPlugin = require('eleventy-plugin-lazyimages')

module.exports = (eleventyConfig) => {
  eleventyConfig.addShortcode('intro', function(message) {
    return `<p class="px-6 text-3xl text-black leading-tight max-w-md" data-aos="fade-right" data-aos-delay="100">${message} <a href="mailto:hi@xmateo.com" class="underline">hi@xmateo.com</a>.</p>`
  })

  eleventyConfig.addPlugin(lazyImagesPlugin)

  // Minify our HTML
  eleventyConfig.addTransform('htmlmin', (content, outputPath) => {
    if (outputPath.endsWith('.html')) {
      let minified = htmlmin.minify(content, {
        useShortDoctype: true,
        removeComments: true,
        collapseWhitespace: true
      })
      return minified
    }
    return content
  })

  // Layout aliases
  eleventyConfig.addLayoutAlias('default', 'layouts/default.njk')

  // Include our static assets
  eleventyConfig.addPassthroughCopy('css')
  eleventyConfig.addPassthroughCopy('fonts')
  eleventyConfig.addPassthroughCopy('js')
  eleventyConfig.addPassthroughCopy('images')
  eleventyConfig.addPassthroughCopy('robots.txt')
  eleventyConfig.addPassthroughCopy('pdf')

  return {
    templateFormats: ['njk'],
    markdownTemplateEngine: 'njk',
    htmlTemplateEngine: 'njk',
    passthroughFileCopy: true,

    dir: {
      input: 'site',
      output: 'dist',
      includes: 'includes',
      data: 'globals'
    }
  }
}
