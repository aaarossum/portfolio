let mix = require('laravel-mix')
let tailwindcss = require('tailwindcss')
require('laravel-mix-purgecss')

// Paths
const paths = {
  sass: {
    source: './resources/sass/main.scss',
    dest: 'css/'
  },
  javascript: {
    source: './resources/js/main.js',
    dest: 'js/'
  }
}

// Run mix
mix
  .webpackConfig()

  // Concatenate & Compile Javascript
  .js(paths.javascript.source, paths.javascript.dest)
  .js('./resources/js/generate.js', paths.javascript.dest)

  // Compile SCSS & TailwindCSS
  .sass(paths.sass.source, paths.sass.dest)
  .options({
    processCssUrls: false,
    postCss: [tailwindcss('tailwind.config.js')]
  })

// Production only

if (mix.inProduction()) {
  // Remove any unused CSS using Purge
  mix
    .purgeCss({
      folders: ['site'],
      extensions: ['njk'],
      whitelist: [
        'body',
        'html',
        'a',
        'h1',
        'h2',
        'h3',
        'h4',
        'p',
        'blockquote',
        'breadcrumbs',
        'content',
        'form',
        'input',
        'textarea',
        'intro',
        'btn',
        'loaded',
        'page-title',
        'required',
        'row',
        'visually-hidden',
        'menu-visible',
        'text-yellow',
        'text-orange',
        'text-red',
        'text-black',
        'triangle',
        'square',
        'smooth-square',
        'circle',
        'rectangle',
        'shape-sm',
        'shape-md',
        'shape-lg',
        'r25',
        'r45',
        'cursor-pointer',
        'hover:opacity-50'
      ],
      whitelistPatterns: [/tech-/, /aos/, /shape/, /top-/]
    })
    // Minifies CSS & JS files
    .minify(paths.sass.dest + 'main.css')
    .minify(paths.javascript.dest + 'main.js')
    .minify(paths.javascript.dest + 'generate.js')
}
