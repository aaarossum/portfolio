/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/generate.js":
/*!**********************************!*\
  !*** ./resources/js/generate.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var button;
var input;
var container;
var preview;
var shapeHTML = '<div></div>';
var previewHTML = '<span></span></br>';
var parameters = [['text-yellow', 'text-orange', 'text-red', 'text-black'], ['triangle', 'square', 'smooth-square', 'circle', 'rectangle'], ['shape-sm', 'shape-md', 'shape-lg'], ['', 'top-1/12', 'top-2/12', 'top-3/12', 'top-4/12', 'top-5/12', 'top-6/12', 'top-7/12', 'top-8/12', 'top-9/12', 'top-10/12', 'top-11/12', 'top-12/12'], ['', 'r25', 'r45']];

function lock(e) {
  if (typeof e != 'undefined') e.preventDefault();
  if (!e.target.classList.contains('a')) return false;
  var isLocked = e.target.classList.contains('locked');
  if (isLocked) e.target.classList.remove('locked');else e.target.classList.add('locked');
}

function randomize(e) {
  if (typeof e != 'undefined') e.preventDefault();
  var shapes = document.querySelectorAll('.shapes div');
  var previews = document.querySelectorAll('.shapes-preview span');

  for (i = 0; i < input.value; i++) {
    if (shapes[i].querySelectorAll('.a.locked').length === 0) {
      var output = '';
      parameters.forEach(function (parameter) {
        output += parameter[Math.floor(Math.random() * parameter.length)] + ' ';
      });
      previews[i].innerHTML = "\"".concat(output, "\",");
      shapes[i].classList = output + ' absolute';
      shapes[i].innerHTML = document.querySelector(".shapes-partials .".concat(output.split(' ')[1])).innerHTML;
      shapes[i].querySelector('.a').classList.add('cursor-pointer', 'hover:opacity-50');
    }
  }
}

function init() {
  preview.innerHTML = '';
  container.innerHTML = '';

  for (var _i = 0; _i < input.value; _i++) {
    container.innerHTML += shapeHTML;
  }

  for (var _i2 = 0; _i2 < input.value; _i2++) {
    preview.innerHTML += previewHTML;
  }

  var shapes = document.querySelectorAll('.shapes div');
  shapes.forEach(function (shape) {
    shape.addEventListener('click', lock);
  });
  randomize();
}

window.addEventListener('load', function (event) {
  button = document.querySelector('.random');
  input = document.querySelector('#count');
  container = document.querySelector('.shapes');
  preview = document.querySelector('.shapes-preview');
  button.addEventListener('click', randomize);
  input.addEventListener('change', init);
  init();
});

/***/ }),

/***/ 1:
/*!****************************************!*\
  !*** multi ./resources/js/generate.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/rossman/Downloads/up/portfolio/resources/js/generate.js */"./resources/js/generate.js");


/***/ })

/******/ });