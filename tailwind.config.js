const plugin = require('tailwindcss/plugin')
const utilities = require('./tailwind.utilities.js')

module.exports = {
  theme: {
    extend: {
      screens: {
        xs: '330px'
      },
      backgroundColor: (theme) => ({
        black: 'var(--black)',
        yellow: 'var(--yellow)',
        orange: 'var(--orange)',
        red: 'var(--red)',
        shadow: '#434343'
      }),
      textColor: {
        black: 'var(--black)',
        yellow: 'var(--yellow)',
        orange: 'var(--orange)',
        red: 'var(--red)',
        shadow: '#434343'
      },
      borderColor: {
        black: 'var(--black)'
      },
      fontSize: {
        huge: '5.5rem'
      },
      height: {
        project: '330px'
      },
      lineHeight: {
        tightest: '0.8'
      }
    }
  },
  variants: {
    opacity: ['responsive', 'hover', 'focus', 'group-hover']
  },
  plugins: [
    plugin(function({ addUtilities }) {
      addUtilities(utilities)
    })
  ]
}
