const AOS = require('aos')

function setThemeButtons(theme) {
  document.querySelectorAll('.theme').forEach((button) => {
    if (button.getAttribute('data-theme') === theme) {
      button.classList.remove('text-outline')
      button.classList.add('text-black')
    } else {
      button.classList.remove('text-black')
      button.classList.add('text-outline')
    }
  })
}

function cleanTheme() {
  document.body.classList.remove('light')
  document.body.classList.remove('dark')
  window.localStorage.setItem('theme', '')
}

function switchTheme(theme) {
  if (!document.body.classList.contains(theme)) {
    cleanTheme()
    document.body.classList.add(theme)
    window.localStorage.setItem('theme', theme)
  }
  setThemeButtons(theme)
}

if (
  window.matchMedia('(prefers-color-scheme: dark)').matches ||
  window.localStorage.getItem('theme') === 'dark'
)
  switchTheme('dark')

document.querySelectorAll('.theme').forEach((button) => {
  button.addEventListener('click', (e) => {
    switchTheme(e.target.getAttribute('data-theme'))
  })
})

window.addEventListener('load', (event) => {
  AOS.init()
})