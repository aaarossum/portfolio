let button
let input
let container
let preview

const shapeHTML = '<div></div>'
const previewHTML = '<span></span></br>'
const parameters = [
  ['text-yellow', 'text-orange', 'text-red', 'text-black'],
  ['triangle', 'square', 'smooth-square', 'circle', 'rectangle'],
  ['shape-sm', 'shape-md', 'shape-lg'],
  [
    '',
    'top-1/12',
    'top-2/12',
    'top-3/12',
    'top-4/12',
    'top-5/12',
    'top-6/12',
    'top-7/12',
    'top-8/12',
    'top-9/12',
    'top-10/12',
    'top-11/12',
    'top-12/12'
  ],
  ['', 'r25', 'r45']
]
function lock(e) {
  if (typeof e != 'undefined') e.preventDefault()
  if (!e.target.classList.contains('a')) return false
  let isLocked = e.target.classList.contains('locked')
  if (isLocked) e.target.classList.remove('locked')
  else e.target.classList.add('locked')
}
function randomize(e) {
  if (typeof e != 'undefined') e.preventDefault()
  const shapes = document.querySelectorAll('.shapes div')
  const previews = document.querySelectorAll('.shapes-preview span')
  for (i = 0; i < input.value; i++) {
    if (shapes[i].querySelectorAll('.a.locked').length === 0) {
      let output = ''
      parameters.forEach(function(parameter) {
        output += parameter[Math.floor(Math.random() * parameter.length)] + ' '
      })
      previews[i].innerHTML = `"${output}",`
      shapes[i].classList = output + ' absolute'
      shapes[i].innerHTML = document.querySelector(
        `.shapes-partials .${output.split(' ')[1]}`
      ).innerHTML
      shapes[i]
        .querySelector('.a')
        .classList.add('cursor-pointer', 'hover:opacity-50')
    }
  }
}
function init() {
  preview.innerHTML = ''
  container.innerHTML = ''
  for (let i = 0; i < input.value; i++) {
    container.innerHTML += shapeHTML
  }
  for (let i = 0; i < input.value; i++) {
    preview.innerHTML += previewHTML
  }
  const shapes = document.querySelectorAll('.shapes div')
  shapes.forEach(function(shape) {
    shape.addEventListener('click', lock)
  })
  randomize()
}

window.addEventListener('load', (event) => {
  button = document.querySelector('.random')
  input = document.querySelector('#count')
  container = document.querySelector('.shapes')
  preview = document.querySelector('.shapes-preview')
  button.addEventListener('click', randomize)
  input.addEventListener('change', init)
  init()
})
