[![Netlify Status](https://api.netlify.com/api/v1/badges/45a7d872-f22b-42eb-949a-5ade432cb5a0/deploy-status)](https://app.netlify.com/sites/hopeful-swartz-bf3340/deploys)

## Requirements

Node `>=` v8.9.0

Project is based on Skeleventy, a boilerplate for 11ty and Tailwind. Additionally, the ESLint package with Prettier plugin is installed for structuring code of JavaScript files. 11ty plugin for lazy loading of the images is installed and used instead of the solution in Skeleventy project. Installed AOS for animations on scroll.

## Installation

```
npm install
```

## Development

```
npm run dev
npm run lintfix
```

## Production

```
npm run production
```
To Do:
- [ ] survey of sdlsdtrbllm
- [ ] social admin python
- [ ] create CV pdf in same style
- [ ] fix k&t - netlify, or only secure
- [ ] villasole - fix date
- [ ] proofreading, feedback etc
- [ ] add twitter, social media meta
- [ ] prettifier for scss / sass?